<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'layouts.pos-home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
