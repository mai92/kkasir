<?php

namespace App\Http\Livewire;

use App\Category;
use Livewire\Component;

class SidebarCategory extends Component
{
    public $categoryId;

    public function mount()
    {
        $this->categoryId = request('category');
    }

    public function render()
    {
        return view('livewire.sidebar-category', [
            'categories' => Category::orderBy('name')->get(),
        ]);
    }

    public function selectCategory($categoryId)
    {
        $this->categoryId = $categoryId;
        $this->emit('changeCategory', $categoryId);
    }
}
