<?php

namespace App\Http\Livewire;

use App\Facades\Cart;
use App\Product;
use Livewire\Component;

class ProductSingle extends Component
{
    public $search;
    public $category;
    public $product;

    protected $updatesQueryString = ['search', 'category'];

    public function mount($product)
    {
        $this->product = $product;
    }
    public function render()
    {
        return view('livewire.product-single');
    }

    public function addToCart($productId)
    {
        Cart::add(Product::find($productId));
        $this->emit('cartAdded');
    }
}
