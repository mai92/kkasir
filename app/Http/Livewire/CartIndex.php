<?php

namespace App\Http\Livewire;

use App\Facades\Cart;
use App\Product;
use Livewire\Component;

class CartIndex extends Component
{
    public $cart;
    public $grandTotal = 0;

    protected $listeners = [
        'cartAdded' => 'updateCartItems',
    ];

    public function mount()
    {
        $this->cart = Cart::get();
        $this->grandTotal = collect($this->cart['products'])->sum('price');
    }

    public function render()
    {
        $items = collect($this->cart['products']);

        $items = $items->groupBy('id')->transform(function($item) {
            return [
                'id' => $item[0]->id,
                'name' => $item[0]->name,
                'qty' => $item->count(),
                'price' => $item[0]->price,
                'total_price' => $item->sum('price')
            ];
        });

        return view('livewire.cart-index', [
            'items' => $items->sortBy('name'),
        ]);
    }

    public function updateCartItems()
    {
        $this->cart = Cart::get();
        $this->grandTotal = collect($this->cart['products'])->sum('price');
    }

    public function decreaseQty($productId)
    {
        Cart::remove($productId);
        $this->updateCartItems();
    }

    public function addQty($productId)
    {
        Cart::add(Product::find($productId));;
        $this->updateCartItems();
    }

    public function removeItem($productId)
    {
        Cart::removeItem($productId);;
        $this->updateCartItems();
    }

    public function getTotalProperty()
    {
        return number_format($this->grandTotal, 0, ",", ".");
    }
}
