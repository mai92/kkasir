<?php

namespace App\Http\Livewire;

use App\Category;
use Livewire\Component;

class Home extends Component
{
    public $categoryId;

    public function render()
    {
        return view('livewire.home', [
            'categories' => Category::orderBy('name')->get(),
        ]);
    }

    public function selectCategory($categoryId)
    {
        $this->categoryId = $categoryId;
        $this->emit('changeCategory', $categoryId);
    }
}
