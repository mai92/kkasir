<?php

namespace App\Http\Livewire;

use App\Product;
use Livewire\Component;
use Livewire\WithPagination;

class ProductIndex extends Component
{
    use WithPagination;

    public $search;
    public $category;

    protected $listeners = [
        'changeCategory',
    ];

    protected $updatesQueryString = ['search', 'category', 'page'];

    public function mount()
    {
        $this->search = request('search');
    }

    public function render()
    {
        $products = new Product();

        if ($this->category !== null) {
            $products = $products->where('category_id', $this->category);
        }

        if ($this->search !== null) {
            $products = $products->where('name','like', '%' . $this->search . '%');
        }

        return view('livewire.product-index', [
            'products' => $products->paginate(12),
        ]);
    }

    public function changeCategory($categoryId)
    {
        $this->category = $categoryId;
    }
}
