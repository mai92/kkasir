<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>KKasir</title>
  <link rel="stylesheet" href="/css/main.css">
  @livewireStyles
</head>

<body class="font-sans bg-gray-300 text-gray-800 antialiased">
  <div class="h-screen">
    <div class="flex flex-col flex-1 h-screen">
      <div class="flex items-center justify-center bg-teal-500 h-18">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag text-white h-10 w-10"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
        <span class="font-bold text-white text-5xl ml-3">KKasir</span>
      </div>

      <div class="flex overflow-y-hidden flex-1">

        <livewire:sidebar-category>

        <livewire:product-index>

        <div class="w-3/12 bg-gray-100 p-4">
          <h2 class="font-bold text-3xl tracking-tight mb-4">Pembelian</h2>

          <div class="p-2 overflow-x-hidden overflow-y-auto h-auto" style="height:700px;">
          {{-- <livewire:sidebar-cart> --}}
          </div>

          <div class="border-t border-gray-500 mt-5 pt-2">
            <div class="flex items-center justify-between">
              <h2 class="font-bold text-2xl justify-start">Total</h2>
              <h2 class="font-bold text-3xl justify-end text-red-600">200.000</h2>
            </div>
            <button class="rounded-lg bg-teal-500 p-2 w-full text-white font-bold hover:opacity-75 h-14 text-2xl mt-4">Bayar</button>
          </div>

        </div>
      </div>
    </div>
  </div>

  @livewireScripts
</body>

</html>
