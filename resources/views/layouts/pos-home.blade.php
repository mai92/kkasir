<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>KKasir</title>
  <link rel="stylesheet" href="/css/main.css">
  @livewireStyles
</head>

<body class="font-sans bg-gray-300 text-gray-800 antialiased">
  <div class="h-screen">
    <div class="flex flex-col flex-1 h-screen">
      <div class="flex items-center justify-center bg-teal-500 h-18">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag text-white h-10 w-10"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
        <span class="font-bold text-white text-5xl ml-3">KKasir</span>
      </div>

      <div class="flex flex-1 overflow-y-hidden">
        <livewire:sidebar-category>
        <livewire:product-index>
        <livewire:cart-index>
      </div>
    </div>
  </div>
  @livewireScripts
</body>

</html>
