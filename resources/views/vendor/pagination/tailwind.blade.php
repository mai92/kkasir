@if ($paginator->hasPages())
    <ul class="flex items-center justify-center p-3 mt-4 text-teal-500" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="bg-teal-500 text-white px-3 py-2 text-xl m-1 border-teal-500 rounded-md" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="" aria-hidden="true">
                    <span class="d-none d-md-block">&lsaquo;</span>
                </span>
            </li>
        @else
            <li class="">
                <button type="button" class="bg-white px-3 py-2 text-xl m-1 border-teal-500 rounded-md hover:bg-teal-500 hover:text-white" wire:click="previousPage" rel="prev" aria-label="@lang('pagination.previous')">
                    <span class="d-none d-md-block">&lsaquo;</span>
                </button>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            {{-- @if (is_string($element))
                <li class="page-item disabled d-none d-md-block" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
            @endif --}}

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="bg-teal-500 text-white px-3 py-2 text-xl m-1 border-teal-500 rounded-md" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                    @else
                        <li class=""><button type="button" class="bg-white px-3 py-2 text-xl m-1 border-teal-500 rounded-md hover:bg-teal-500 hover:text-white" wire:click="gotoPage({{ $page }})">{{ $page }}</button></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li>
                <button type="button" class="bg-white px-3 py-2 text-xl m-1 border-teal-500 rounded-md hover:bg-teal-500 hover:text-white" wire:click="nextPage" rel="next" aria-label="@lang('pagination.next')">
                    <span class="d-none d-md-block">&rsaquo;</span>
                </button>
            </li>
        @else
            <li class="bg-teal-500 text-white px-3 py-2 text-xl m-1 border-teal-500 rounded-md" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">
                    <span class="d-none d-md-block">&rsaquo;</span>
                </span>
            </li>
        @endif
    </ul>
@endif
