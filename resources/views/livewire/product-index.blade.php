<div class="w-7/12 overflow-x-hidden h-auto overflow-y-auto p-4">
<input wire:model="search" class="bg-white focus:outline-none focus:shadow-outline border border-gray-500 rounded-full py-2 px-4 block w-full appearance-none leading-normal mb-4" name=" " type="text" placeholder="Cari Produk">
  <div class="flex justify-center m-4">
    <span wire:loading>Sedang mencari ....</span>
  </div>
  <div class="grid lg:grid-cols-4 gap-4">
    @forelse($products as $product)
       <livewire:product-single wire:loading.remove :product="$product" :key="$product->id">
    @empty

    @endforelse
  </div>
  {{ $products->withQueryString()->links('vendor.pagination.tailwind') }}
</div>
