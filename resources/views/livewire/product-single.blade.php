<div class="bg-white shadow-md border-solid border-gray-700 p-3 rounded-md">
  <img src="https://awsimages.detik.net.id/community/media/visual/2019/06/25/4b2e9e97-a47e-49d3-8e20-26074032b26f_11.jpeg?w=700&q=80" class="rounded-sm hover:opacity-75 transition duration-200" alt="">
  <h2 class="font-bold text-xl my-2">{{ $product->name }}</h2>
  <button wire:click="addToCart({{$product->id}})" class="rounded-lg bg-teal-500 p-2 w-full text-white font-bold hover:opacity-75 active:bg-orange-500">Tambahkan</button>
</div>
