<div class="w-2/12 bg-gray-100 p-4">
  <h2 class="font-bold text-3xl tracking-tight mb-4">Kategori</h2>

  @foreach($categories as $category)
    <a wire:click="selectCategory({{ $category->id }})" :key="$category->id" class=" border border-teal-500 shadow shadow-md block p-4 mb-2 rounded-md hover:bg-teal-500 hover:text-white font-bold  @if($category->id == $categoryId)
      bg-teal-500
     @else
     bg-white
    @endif">
        {{ $category->name }}
    </a>
  @endforeach
</div>
