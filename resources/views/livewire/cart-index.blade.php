<div class="w-3/12 bg-gray-100 p-4">
<h2 class="font-bold text-3xl tracking-tight mb-4">Pembelian</h2>

<div class="p-2 overflow-x-hidden overflow-y-auto h-auto" style="height:700px;">
@forelse($items as $item)
<div class="flex justify-between bg-white shadow shadow-lg border border-gray-100 flex-1 rounded-lg mb-2">
  <div>
     <img src="https://awsimages.detik.net.id/community/media/visual/2019/06/25/4b2e9e97-a47e-49d3-8e20-26074032b26f_11.jpeg?w=700&q=80" class="rounded-r-none rounded-lg h-24 w-24" alt="">
  </div>
  <div class="flex flex-1 flex-col ml-4">
    <h2 class="font-bold">{{ $item['name'] }} x {{ $item['qty'] }} @ {{ $item['price'] }}</h2>
    <div class="mb-4">
    <button wire:click="decreaseQty({{ $item['id'] }})" class="py-1 px-2 bg-red-400 active:bg-orange-500 hover:opacity-75 rounded rounded-lg w-8 h-8"><span class="text-white">-</span></button>
    <button wire:click="addQty({{ $item['id'] }})" class="py-1 px-2 bg-teal-400 active:bg-orange-500 hover:opacity-75 rounded rounded-lg w-8 h-8"><span class="text-white">+</span></button>
    </div>
  </div>
  <div class="flex items-start mr-2">
    <p class="font-bold text-red-600">Rp. {{ number_format($item['total_price'], 0, ",", ".") }}</p>
  </div>
  <button wire:click="removeItem({{ $item['id'] }})" class="float-right justify-end py-1 px-2 w-8 bg-red-400 hover:bg-red-600 rounded-l-none rounded rounded-lg">
    <span class="text-white">x</span>
  </button>
</div>
@empty
  <div class="flex justify-start items-center rounded-md bg-white shadow shadow-lg border border-gray-100 flex-1 rounded-lg mb-2 p-4">
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left w-6 h-6 text-gray-600"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>
    <span class="text-2xl text-gray-600">Pilih produk</span>
  </div>
@endforelse
</div>

<div class="border-t border-teal-500 mt-5 pt-2">
<div class="flex items-center justify-between">
<h2 class="font-bold text-2xl justify-start">Total</h2>
<h2 class="font-bold text-3xl justify-end text-red-600">{{ $this->total }}</h2>
</div>
<button
class="rounded-lg bg-orange-500 p-2 w-full text-white font-bold hover:opacity-75 ease-in-out transition duration-75 h-14 text-2xl mt-4">Bayar</button>
</div>

</div>
