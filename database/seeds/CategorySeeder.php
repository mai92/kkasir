<?php

use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Boba',
            'slug' => Str::slug('boba'),
        ]);

        Category::create([
            'name' => 'Thai Tea',
            'slug' => Str::slug('Thai Tea'),
        ]);

        Category::create([
            'name' => 'Coffe',
            'slug' => Str::slug('Coffe'),
        ]);
    }
}
