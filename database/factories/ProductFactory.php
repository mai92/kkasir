<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Product::class, function (Faker $faker) {
    $categoryId = Category::inRandomOrder()->first()->id;

    return [
        'category_id' => $categoryId,
        'name' => $name = $faker->sentence(2),
        'slug' => Str::slug($name),
        'description' => $faker->sentence(20),
        'price' => rand(100, 200),
        'image' => null,
    ];
});
